package models

import (
	"errors"
	"fmt"
	"github.com/beego/beego/v2/client/orm"
	"golang.org/x/crypto/bcrypt"
	"time"
)

var (
	UserList map[string]*User
)

func init() {
	orm.RegisterModel(new(User))
}

type User struct {
	Id        int       `orm:"column(id);pk"`
	Username  string    `orm:"column(username)" json:"username"`
	Password  string    `orm:"column(password)" json:"-"`
	Name      string    `orm:"column(name)" json:"name"`
	Email     string    `orm:"column(email)" json:"email"`
	IsActive  int       `orm:"column(isActive)" json:"isActive"`
	CreatedAt time.Time `orm:"column(created_at);auto_now_add;type(date)" json:"createdAt"`
	UpdatedAt time.Time `orm:"column(updated_at);auto_now_add;type(date)" json:"updatedAt"`
}

func (User) TableName() string {
	return "users"
}

type JsonAddLogin struct {
	Username string `orm:"column(username)" json:"username"`
	Password string `orm:"column(password)" json:"password"`
	Name     string `orm:"column(name)" json:"name"`
	Email    string `orm:"column(email)" json:"email"`
}

func AddUser(u *JsonAddLogin) (*User, error) {
	/*u.Id = "user_" + strconv.FormatInt(time.Now().UnixNano(), 10)
	UserList[u.Id] = &u*/
	o := orm.NewOrm()
	to, _ := o.Begin()
	dataCreated := User{
		IsActive: 1,
		Username: u.Username,
		Name:     u.Name,
		Email:    u.Email,
		Password: u.Password,
	}
	var data User
	if err := to.QueryTable(new(User)).Filter("username", u.Username).One(&data); err == nil {
		to.Rollback()
		return nil, fmt.Errorf("username sudah tersedia")
	}
	//m.Created_at = time.Now()
	hash, err := bcrypt.GenerateFromPassword([]byte(u.Password), bcrypt.DefaultCost)
	if err != nil {
		return nil, err
	}
	u.Password = string(hash)
	_, err = to.Insert(&dataCreated)
	if err != nil {
		to.Rollback()
		return nil, err
	}

	to.Commit()

	return &dataCreated, nil
}

func GetUser(uid string) (u *User, err error) {
	if u, ok := UserList[uid]; ok {
		return u, nil
	}
	return nil, errors.New("User not exists")
}

/*func GetAllUsers() map[string]*User {
	return UserList
}

func UpdateUser(uid string, uu *User) (a *User, err error) {
	if u, ok := UserList[uid]; ok {
		if uu.Username != "" {
			u.Username = uu.Username
		}
		if uu.Password != "" {
			u.Password = uu.Password
		}
		if uu.Profile.Age != 0 {
			u.Profile.Age = uu.Profile.Age
		}
		if uu.Profile.Address != "" {
			u.Profile.Address = uu.Profile.Address
		}
		if uu.Profile.Gender != "" {
			u.Profile.Gender = uu.Profile.Gender
		}
		if uu.Profile.Email != "" {
			u.Profile.Email = uu.Profile.Email
		}
		return u, nil
	}
	return nil, errors.New("User Not Exist")
}*/

func Login(username, password string) bool {
	/*for _, u := range UserList {
		if u.Username == username && u.Password == password {
			return true
		}
	}*/
	return false
}

func DeleteUser(uid string) {
	delete(UserList, uid)
}
