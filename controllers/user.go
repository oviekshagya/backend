package controllers

import (
	"backend/models"
	"encoding/json"

	beego "github.com/beego/beego/v2/server/web"
)

// Operations about Users
type UserController struct {
	beego.Controller
}

// Post @Title CreateUser
// @Description create users
// @Param	body		body 	models.User	true		"body for user content"
// @Success 201 {int} models.User.Id
// @Failure 400 body is empty
// @router / [post]
func (u *UserController) Post() {
	var user models.JsonAddLogin
	err := json.Unmarshal(u.Ctx.Input.RequestBody, &user)
	if err != nil {
		u.Ctx.Output.JSON(map[string]interface{}{
			"status":  false,
			"message": err.Error(),
		}, true, true)
		return
	}
	_, errModels := models.AddUser(&user)
	if err != nil {
		u.Ctx.Output.JSON(map[string]interface{}{
			"status":  false,
			"message": errModels.Error(),
		}, true, true)
		return
	}
	u.Ctx.Output.JSON(map[string]interface{}{
		"status":  true,
		"message": "Sukses",
		"data":    user,
	}, true, true)
	return
}

// Get @Title Get
// @Description get user by uid
// @Param	uid		path 	string	true		"The key for staticblock"
// @Success 200 {object} models.User
// @Failure 403 :uid is empty
// @router /:uid [get]
func (u *UserController) Get() {
	uid := u.GetString(":uid")
	if uid != "" {
		user, err := models.GetUser(uid)
		if err != nil {
			u.Data["json"] = err.Error()
		} else {
			u.Data["json"] = user
		}
	}
	u.ServeJSON()
}

// Login @Title Login
// @Description Logs user into the system
// @Param	username		query 	string	true		"The username for login"
// @Param	password		query 	string	true		"The password for login"
// @Success 200 {string} login success
// @Failure 403 user not exist
// @router /login [get]
func (u *UserController) Login() {
	username := u.GetString("username")
	password := u.GetString("password")
	if models.Login(username, password) {
		u.Data["json"] = "login success"
	} else {
		u.Data["json"] = "user not exist"
	}
	u.ServeJSON()
}

// Logout @Title logout
// @Description Logs out current logged in user session
// @Success 200 {string} logout success
// @router /logout [get]
func (u *UserController) Logout() {
	u.Data["json"] = "logout success"
	u.ServeJSON()
}
