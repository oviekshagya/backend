package conf

import (
	"fmt"
	"github.com/beego/beego/v2/client/orm"
	"github.com/beego/beego/v2/core/config"
	_ "github.com/go-sql-driver/mysql"
)

type EnvRead struct {
	DbDrive    string
	DbUser     string
	DbPassword string
	DBHost     string
	DBName     string
}

func Connection() {
	conf, err := config.NewConfig("ini", "conf/app.conf")
	if err != nil {
		fmt.Println("Error reading config file:", err)
		return
	}
	dbDriver, _ := conf.String("db.driver")
	dbUser, _ := conf.String("db.user")
	dbPassword, _ := conf.String("db.password")
	dbURL, _ := conf.String("db.url")
	dbName, _ := conf.String("db.name")

	// Inisialisasi Beego ORM
	orm.RegisterDriver("mysql", orm.DRMySQL)
	errConn := orm.RegisterDataBase("default", dbDriver, fmt.Sprintf("%s:%s@tcp(%s:3306)/%s?charset=utf8mb4&parseTime=True&loc=Local", dbUser, dbPassword, dbURL, dbName))

	if errConn != nil {
		fmt.Println("DISCONNECT DATA BASE")
		return
	}
	orm.Debug = true
	fmt.Println("CONNECTED")
	// AutoMigrate - Migrasi otomatis untuk membuat tabel
	/*errSync := orm.RunSyncdb("default", false, true)
	if errSync != nil {
		fmt.Println("err Sync", errSync.Error())
	}*/
}
