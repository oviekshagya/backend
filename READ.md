# Beego v2 Framework
by Oviek Shagya Ghinulur

## Requirement

* [GoLang](https://golang.org/)
* [mysql](https://www.mysql.org/)
* [Beego Orm](https://beego.wiki/docs/mvc/model/overview/)

note: Project ini dibuat menggunakan env localhost untuk kebutuhan proses requitment


## How to Run
Download package untuk kebutuhan framework, mysql, beego, orm
```sh
$ go mod tidy
```

Generate router before run app
```sh
$ bee generate routers
```

Run App dengan bee tools
```sh
$ bee run
```

Run App dengan go
```sh
$ go run main.go
```
